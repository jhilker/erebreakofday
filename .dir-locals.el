;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((eval . (progn
                   (setq-local org-roam-directory "~/Projects/conworlds/brokenThrones/content-org")
                   (setq-local org-roam-db-location "~/Projects/conworlds/brokenThrones/org-roam.db")))))
("content-org/"  . ((org-mode . ((eval . (org-hugo-auto-export-mode)))))))
